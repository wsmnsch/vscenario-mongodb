const _ = require('lodash')
const Pipeline = require('./pipeline')
const { ObjectId } = require('mongodb')

module.exports = async function ({
    db,
    phases,
    DOCS_COLLECTION_NAME,
    SCENARIOS_COLLECTION_NAME = 'scenarios',
    PHASE_FIELD_NAME = 'phase',
    SCENARIO_FIELD_NAME = 'scenario',
}) {

    if (!db) {
        throw new Error('@vscenario db is required')
    }

    if (!_.size(phases)) {
        console.warn('@vscenario empty phases array')
    }

    if (!DOCS_COLLECTION_NAME) {
        throw new Error('@vscenario DOCS_COLLECTION_NAME is required')
    }


    async function getPipeline (scenarioId) {
        const scenario = await db.collection(SCENARIOS_COLLECTION_NAME).findOne({
            _id: scenarioId
        })

        if (!scenario) {
            throw new Error(`Can't find scenario ${scenarioId}`)
        }

        return new Pipeline({
            fieldName: PHASE_FIELD_NAME,
            scenario: scenario.pipeline,
            phases,
        })
    }

    async function nextPhase (doc, nextPhaseId) {

        const pipeline = await getPipeline(_.get(doc, SCENARIO_FIELD_NAME))
        const currentPhase = await pipeline.getDocPhase(doc)

        if (currentPhase && typeof currentPhase.finalize === 'function') {
            await currentPhase.finalize.call(this, doc, currentPhase.params)
        }

        const phase = nextPhaseId ? await pipeline.getDocPhase(doc, nextPhaseId) : await pipeline.next(doc)

        if (!phase) {
            console.warn(new Date(), `nextPhase not found for doc ${doc._id}`)
            return
        }

        await savePhase(doc, phase.id)

        if (typeof phase.start !== 'function') { return }

        doc = await db.collection(DOCS_COLLECTION_NAME).findOne({_id: doc._id})

        return phase.start.call(this, doc, {}, phase.params)
    }

    async function startPhase (doc, options) {
        const pipeline = await getPipeline(_.get(doc, SCENARIO_FIELD_NAME))
        const isStartable = await pipeline.isStartable(doc)

        if (!isStartable) { return }

        const phase = pipeline.getDocPhase(doc)

        if (typeof phase.start !== 'function') { return }

        doc = await db.collection(DOCS_COLLECTION_NAME).findOne({_id: doc._id})

        return phase.start.call(this, doc, options, phase.params)
    }

    async function savePhase (doc, phaseId) {

        const newPhase = {
            _id: new ObjectId(),
            [PHASE_FIELD_NAME]: phaseId,
            date: new Date(),
        }

        const update = {
            $set: {
                [PHASE_FIELD_NAME]: phaseId
            },
            $push: {
                [`${PHASE_FIELD_NAME}Meta.changes`]: newPhase,
            },
        }

        const lastPhaseChange = getLastPhaseChange(doc)

        if (lastPhaseChange) {
            update['$inc'] = {
                [`${PHASE_FIELD_NAME}Meta.metrics.${lastPhaseChange.pstage}`]: Date.now() - lastPhaseChange.date.getTime(),
            }
        }

        return await db.collection(DOCS_COLLECTION_NAME).updateOne({ _id: doc._id }, update)
    }

    function getLastPhaseChange (doc) {
        return _.last(_.get(doc, `${PHASE_FIELD_NAME}Meta.changes`, []))
    }

    function getAllowedOps (doc) {
        const phase = _.find(phases, { id: _.get(doc, PHASE_FIELD_NAME)})

        if (!phase) { return [] }

        return _.isFunction(phase.allowedOps) ? phase.allowedOps(doc) : phase.allowedOps
    }

    async function getRequirements (doc, opName) {
        const phase = _.find(phases, { id: _.get(doc, PHASE_FIELD_NAME)})

        if (!phase) { return [] }

        if (typeof phase.requirements == 'function') {
            return await phase.requirements.call(this, doc, opName)
        }

        if (opName) { return [] }

        return phase.requirements || [
            {
                title: phase.description,
                ok: false
            }
        ]
    }

    async function onOp (op, doc, subdoc, subdocId) {
        const phase = _.find(phases, { id: _.get(doc, PHASE_FIELD_NAME)})

        if (!phase) { return }
        if (typeof phase.onOp !== 'function') { return }

        return await phase.onOp.call(this, op, doc, subdoc, subdocId, phase.params)
    }

    async function onTask (doc, task) {
        const phase = _.find(phases, { id: _.get(doc, PHASE_FIELD_NAME)})

        if (!phase) { return }
        if (typeof phase.onTask !== 'function') { return }

        return await phase.onTask.call(this, task, doc)
    }

    function getPhase (doc) {
        return _.find(phases, { id: _.get(doc, PHASE_FIELD_NAME)})
    }

    return {
        getPipeline,
        nextPhase,
        startPhase,
        savePhase,
        getLastPhaseChange,
        getAllowedOps,
        getRequirements,
        onOp,
        onTask,
        getPhase,
    }

}