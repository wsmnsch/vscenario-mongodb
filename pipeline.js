'use strict'

const _ = require('lodash')

module.exports = Pipeline

function Pipeline (params = { phases: [], scenario: [], fieldName: 'phase' }) {

    this.phases = params.phases || []
    this.scenario = params.scenario || []
    this.phaseFieldName = params.fieldName || 'phase'

    return this
}

Pipeline.prototype.next = async function (doc, fromPhase) {
    fromPhase = fromPhase && fromPhase.id || fromPhase
    const phase = fromPhase || _.get(doc, this.phaseFieldName)
    const currentPhaseIndex = _.findIndex(this.scenario, function (p) {
        return (p && p.id || p) == phase
    })
    const nextPhaseId = this.scenario[(currentPhaseIndex === -1) ? 0 : currentPhaseIndex + 1]
    const nextPhase = this.getPhase(nextPhaseId)

    if (!nextPhase) { return null }

    if (typeof nextPhase.isSuitable === 'function') {

        const isSuitable = await nextPhase.isSuitable(doc, nextPhase.params)

        if (!isSuitable) {
            return this.next(doc, nextPhase.id)
        }
    }

    if (typeof nextPhase.isPassed === 'function') {

        const isPassed = await nextPhase.isPassed(doc, nextPhase.params)

        if (isPassed) {
            return this.next(doc, nextPhase.id)
        }
    }

    return nextPhase
}

Pipeline.prototype.isStartable = async function (doc) {
    const phaseId = _.get(doc, this.phaseFieldName)
    const phase = this.getPhase(phaseId)

    if (!phase) { return false }

    if (typeof phase.start !== 'function') { return false }

    if (_.isFunction(phase.isSuitable)) {

        const isSuitable = await phase.isSuitable(doc, phase.params)

        if (!isSuitable) { return false }
    }

    if (_.isFunction(phase.isProcessed)) {

        const isProcessed = await phase.isProcessed(doc, phase.params)

        if (isProcessed) { return false }
    }

    return true

}


Pipeline.prototype.getPhase = function (phaseId) {

    phaseId = (phaseId && phaseId.id) ? phaseId.id : phaseId

    if (!phaseId && phaseId !== 0) { return null }

    const inScenarioPhase = this.getInScenarioPhase(phaseId)

    if (!inScenarioPhase) { return null }

    const phase = _.find(this.phases, function (phase) {
        return phase == phaseId || (phase && phase.id == phaseId)
    })

    if (!phase) { return null }

    return Object.assign(phase, {
        params: _.get(inScenarioPhase, 'params', {})
    })
}

Pipeline.prototype.getDocPhase = function (doc, phaseId) {

    phaseId = (phaseId && phaseId.id) ? phaseId.id : phaseId

    if (!phaseId && phaseId !== 0) {
        phaseId = _.get(doc, this.phaseFieldName)
    }

    return this.getPhase(phaseId)
}


Pipeline.prototype.getInScenarioPhase = function (phaseId) {
    return _.find(this.scenario, function (phase) {
        return phase == phaseId || (phase && phase.id == phaseId)
    })
}