# VSCENARIO

Allowes to describe handle docs phases and run it in sequence.

## Describe phase

#### **`confirmDoc.js`**
```javascript
function (config) {
    return {
        id: 'confirmDoc',
        description: 'Wait for confirm document operation',
        start: function (doc) {

        },
        onOp: function (op, doc) {
            if (op.name == '')
        }
    }
}
```

## Init vscenario

```javascript
const { MongoClient } = require('mongodb')
const mongoClient = MongoClient.connect('127.0.0.1:27017')
const db = mongoClient.db('test')

const vscenarion = require('vscenario-mongodb')({
    db,
    DOCS_COLLECTION_NAME: 'docs'
    phases: [
        require('./confirmDoc')
    ],
})
```
